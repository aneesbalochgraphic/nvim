require "user.lsp-config" --options
require "user.mason" 
require "user.mason-lsp-config" 
require "user.vs-code-theme" 
require "user.cmp-nvim-lsp"
require "user.nvim-cmp"
require "user.keymaps"
require "user.telescope"
require "user.nvim-tree-filemanager"
require "user.treesitter"
require "user.indent-guide"
require "user.lualine"
require "user.set-options"
require "user.emmet"
require "user.neoterm"



-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- Iocns
  use 'nvim-tree/nvim-web-devicons'
  -- Nvim auto complete 

  use {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",
    'hrsh7th/nvim-cmp',
    "hrsh7th/cmp-nvim-lsp",
    'saadparwaiz1/cmp_luasnip', -- Snippets source for nvim-cmp
    'L3MON4D3/LuaSnip', -- Snippets plugin
    'mhartington/formatter.nvim', -- Formatter

  }
    

  -- Color thems
  use 'Mofiqul/vscode.nvim'
  use 'Abstract-IDE/Abstract-cs'


  

  -- Fuzzy finder telescope 
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.8',
    -- or                            , branch = '0.1.x',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  -- Nvim tree a filemanager
  use "nvim-tree/nvim-tree.lua"

   -- Status line 
   use {
    'nvim-lualine/lualine.nvim',
  }

   -- Buffer tabs manager line 
   use 'lewis6991/gitsigns.nvim' -- For git status
   use 'romgrk/barbar.nvim' -- Buffer Status line : barber


   -- curssor plug
   use "yamatsum/nvim-cursorline"


   -- Autopari
   use {
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    config = function()
        require("nvim-autopairs").setup {}
    end
   }


   -- Tree sitter
   use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
   }
   

   -- Indent guide
   use "lukas-reineke/indent-blankline.nvim"

   -- Commenter 
   use {
        'numToStr/Comment.nvim',
        config = function()
            require('Comment').setup()
        end
    }

  -- Terminal 
  use {"akinsho/toggleterm.nvim", tag = '*', config = function()
    require("toggleterm").setup()
  end}

  use 'LoricAndre/OneTerm.nvim'

  -- Commenter 
  use 'b3nj5m1n/kommentary'

end)



