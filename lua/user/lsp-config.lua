-- Setup LSP
local lspconfig = require('lspconfig')
 
-- List of language servers to setup
local servers = { 'pyright', 'ts_ls', 'lua_ls', 'emmet_ls', 'html', 'cssls', 'eslint' }  -- Add more servers as needed
-- 
 -- Basic configuration for each server
 for _, server in ipairs(servers) do
     lspconfig[server].setup {}
 end
 -- Keybindings for LSP functionality
 local on_attach = function(client, bufnr)
     local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
     local opts = { noremap=true, silent=true }
 
     -- LSP keybindings
     buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)       -- Go to definition
     buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)              -- Hover
     buf_set_keymap('n', 'gi', '<Cmd>lua vim.lsp.buf.implementation()<CR>', opts)   -- Go to implementation
     buf_set_keymap('n', '<C-k>', '<Cmd>lua vim.lsp.buf.signature_help()<CR>', opts) -- Signature help
     buf_set_keymap('n', '<leader>rn', '<Cmd>lua vim.lsp.buf.rename()<CR>', opts)    -- Rename
     buf_set_keymap('n', '<leader>ca', '<Cmd>lua vim.lsp.buf.code_action()<CR>', opts) -- Code action
     buf_set_keymap('n', 'gr', '<Cmd>lua vim.lsp.buf.references()<CR>', opts)        -- References
 end
 
 -- Re-apply the on_attach function to each server
 for _, server in ipairs(servers) do
     lspconfig[server].setup {
         on_attach = on_attach,
     }
 end
