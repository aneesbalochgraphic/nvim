-- Enable syntax highlighting
vim.cmd('syntax on')

-- Enable line numbers
vim.opt.number = true
vim.opt.relativenumber = true

-- Enable auto indentation
vim.opt.autoindent = true
vim.opt.smartindent = true

-- Set tab width
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true  -- Convert tabs to spaces

-- Enable mouse support
vim.opt.mouse = 'a'

-- Set clipboard to use system clipboard
vim.opt.clipboard = 'unnamedplus'

-- Show command in the last line
vim.opt.showcmd = true

-- Enable highlighting for search results
vim.opt.hlsearch = true
vim.opt.incsearch = true


